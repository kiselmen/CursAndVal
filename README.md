# CursAndVal

Для запуска Back:

composer install
В файле .env если надо поправить настройки для БД
    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=test_curs
    DB_USERNAME=root
    DB_PASSWORD=root
Восстановить из дапмпа БД test_curs.sql
php artisan serve

Для запуска Front:

npm install
В файле .env если надо поправить адрес back
  VUE_APP_API_URL=http://localhost:8000/
npm run serve
