<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Valute;
use App\Models\Curse;
use Illuminate\Support\Facades\Http;
use Validator;

class CursController extends Controller
{

    public function from_bank_val(Request $request)
    {
      $result['data'] = null;
      $result['succsess'] = 0;
      $result['error'] = '';
      try {
          $xmlstring = Http::get('http://www.cbr.ru/scripts/XML_val.asp?d=0');
          // $xmlstring = file_get_contents('http://www.cbr.ru/scripts/XML_val.asp?d=0');
      } catch (Exeption $e){
          $result['succsess'] = 0;
          $result['error'] = $e->getMessage();
      }
      if ($xmlstring) {
          $xml_file = simplexml_load_string($xmlstring);
          $json = json_encode($xml_file );
          $array = json_decode($json,TRUE);
          $result_array = [];

          if (array_key_exists('Item', $array)) {
              for ($i = 0; $i < count($array['Item']); $i++){
                  $elem = $array['Item'][$i];
                  // dd($elem);
                  $curVal = Valute::find($elem['@attributes']['ID']);
                  if ($curVal) {
                      $curVal->ID         = $elem['@attributes']['ID'];
                      $curVal->Name       = $elem['Name'];
                      $curVal->EngName    = $elem['EngName'];
                      $curVal->Nominal    = $elem['Nominal'];
                      $curVal->ParentCode = $elem['ParentCode'];
                      $curVal->save();
                      array_push($result_array, $curVal);
                  } else {
                      $newVal = Valute::create([
                      'ID'          => $elem['@attributes']['ID'],
                      'Name'        => $elem['Name'],
                      'EngName'     => $elem['EngName'],
                      'Nominal'     => $elem['Nominal'],
                      'ParentCode'  => $elem['ParentCode']]);
                      array_push($result_array, $newVal);
                  }
              }
          }
          $result['data'] = $result_array;
          $result['succsess'] = 1;
      }

      return response()->json($result);
    }

    public function from_bank_curs(Request $request)
    {
      $dateFormat = 'Y-m-d';
      $validator = Validator::make($request->all(), [
        'date_req1' => 'bail|required|regex:{10}|date_format:'.$dateFormat,
        'date_req2' => 'bail|required|regex:{10}|date_format:'.$dateFormat.'|after:'.date_create($request->date_req1?$request->date_req1:$request->date_req2)->format($dateFormat),
        'VAL_NM_RQ' => 'required',
      ]);

      $result['data'] = null;
      $result['succsess'] = 0;
      $result['error'] = '';

      if ($validator->fails()) {
          $err_str = '';
          foreach ($validator->errors()->all() as $message) {
              $err_str = $err_str.' '.$message;
          }
          $result['error'] = $err_str;
          return response()->json($result);
      } else {

      // $isparam = isset($request->date_req1)&&isset($request->date_req2)&&isset($request->VAL_NM_RQ);
      //
      // if (!$isparam) {
      //    // $date_from = date('Y-m-d');
      //    $result['succsess'] = 0;
      //    $result['error'] = 'Неверные параметры запроса. Используйте date_req1 date_req2 VAL_NM_RQ';
      //    $result['data'] = null;
      //    return response()->json($result);
      // }
      // // dd(strlen($request->date_req1)<9);
      // $isDate1 = (strlen($request->date_req1)>7)&&(strlen($request->date_req1)<11);
      // $isDate2 = (strlen($request->date_req2)>7)&&(strlen($request->date_req2)<11);
      // $isDate = $isDate1&&$isDate2;
      // if (!$isDate) {
      //    $result['succsess'] = 0;
      //    $result['error'] = 'Неверные параметры запроса. Не являются датой date_req1 date_req2 ';
      //    $result['data'] = null;
      //    return response()->json($result);
      // }
      //
          $Val_ID = $request->VAL_NM_RQ;

          $date_from = date('d/m/Y', strtotime($request->date_req1));
          $date_to = date('d/m/Y', strtotime($request->date_req2));

          $param = '?date_req1=' . $date_from . '&date_req2=' . $date_to . '&VAL_NM_RQ=' . $Val_ID;
          try {
              $xmlstring = Http::get('http://www.cbr.ru/scripts/XML_dynamic.asp'.$param);
              // $xmlstring = file_get_contents('http://www.cbr.ru/scripts/XML_dynamic.asp'.$param);
          } catch (Exeption $e){
              $result['succsess'] = 0;
              $result['error'] = $e->getMessage();
          }
          if ($xmlstring) {
              $xml_file = simplexml_load_string($xmlstring);
              $json = json_encode($xml_file );
              $array = json_decode($json,TRUE);
              $result_array = [];

              if (array_key_exists('Record', $array)) {
              // if ($array['Record']) {
                  for ($i = 0; $i < count($array['Record']); $i++){
                      $elem = $array['Record'][$i];
                      // dd($elem);
                      $search = [];
                      array_push($search , ['valute_ID', '=', $elem['@attributes']['Id']]);
                      array_push($search , ['Date', '=', date('Y-m-d', strtotime($elem['@attributes']['Date'])) ]);
                      $curCurs = Curse::where($search)->first();
                      if ($curCurs) {
                          // $curCurs->valute_ID  = $elem['@attributes']['Id'];
                          // $curCurs->Date       = $elem['@attributes']['Date'];
                          $curCurs->Nominal    = $elem['Nominal'];
                          $curCurs->Value      = str_replace(',','.',$elem['Value']);
                          $curCurs->save();
                          array_push($result_array, $curCurs);
                      } else {
                          $newCurs = Curse::create([
                            // date('d/m/Y', strtotime($request->date_req2))
                            'Date'        => date('Y-m-d', strtotime($elem['@attributes']['Date'])),
                            'valute_ID'   => $elem['@attributes']['Id'],
                            'Nominal'     => $elem['Nominal'],
                            'Value'       => str_replace(',','.',$elem['Value'])
                          ]);
                          array_push($result_array, $newCurs);
                      }
                  }
              }
              $result['data'] = $result_array;
              $result['succsess'] = 1;
          }
      }

      return response()->json($result);
    }

}
