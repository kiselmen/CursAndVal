<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Curse extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'id', 'valute_ID', 'Date', 'Value', 'Nominal',
    ];

}
