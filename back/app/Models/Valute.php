<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Valute extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'ID', 'EngName', 'Name', 'Nominal', 'ParentCode', 
    ];

}
