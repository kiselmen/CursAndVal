<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
//use App\Http\Requests\CursRequest;
use App\Models\Curse;
use App\Http\Controllers\API\BaseController as BaseController;
use Validator;

class ApiCursController extends BaseController
{

    public function index(Request $request)
    {

      $dateFormat = 'Y-m-d';
      $validator = Validator::make($request->all(), [
        'date_from' => 'bail|required|regex:{10}|date_format:'.$dateFormat,
        'date_to' => 'bail|required|regex:{10}|date_format:'.$dateFormat.'|after:'.date_create($request->date_from?$request->date_from:$request->date_to)->format($dateFormat),
        'valueID' => 'required',
      ]);

      if($validator->fails()){
          return $this->sendError($validator->errors()->all());
      };

      // $isparam = isset($request->date_from)&&isset($request->date_to)&&isset($request->valueID);
      // // dd(isset($request->valueID));
      // if (!$isparam) {
      //    // $date_from = date('Y-m-d');
      //    $result['succsess'] = 0;
      //    $result['error'] = 'Неверные параметры запроса. Используйте date_from date_to ValueID';
      //    $result['data'] = null;
      //    return response()->json($result);
      // }
      // // dd(strlen($request->date_from)<9);
      // $isDate1 = (strlen($request->date_from)>7)&&(strlen($request->date_from)<11);
      // $isDate2 = (strlen($request->date_to)>7)&&(strlen($request->date_to)<11);
      // $isDate = $isDate1&&$isDate2;
      // if (!$isDate) {
      //    $result['succsess'] = 0;
      //    $result['error'] = 'Неверные параметры запроса. Не являются датой date_from date_to';
      //    $result['data'] = null;
      //    return response()->json($result);
      // }

      $search = [];
      array_push($search , ['Valute_id', '=', $request->valueID]);
      array_push($search , ['Date', '>=', date('Y-m-d', strtotime($request->date_from))]);
      array_push($search , ['Date', '<=', date('Y-m-d', strtotime($request->date_to))]);
      $curCurs = Curse::where($search)->get();
      // $result['data'] = $curCurs;
      // $result['succsess'] = 1;
      // return response()->json($result);
      return $this->sendResponse($curCurs);
    }
}
