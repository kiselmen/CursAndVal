<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;
class BaseController extends Controller
{
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendResponse($result)
    {
      $response = [
            'succsess' => 1,
            'data'    => $result,
            'error'   => '',
        ];
        return response()->json($response, 200);
    }
    /**
     * return error response.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendError($errorMessages = [], $code = 200)
    {
      $response = [
            'succsess' => 0,
            'data'    => null,
        ];
        if(!empty($errorMessages)){
            // dd($errorMessages);
            $err_str = '';
            foreach ($errorMessages as $message) {
                // dd($message);
                $err_str = $err_str.' '.$message;
            }
            $response['error'] = $err_str;
        }
        return response()->json($response, $code);
    }
}
